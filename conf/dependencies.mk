# Where is xerces2?
dep-meta-xerces2 = \
	/usr/share/java/serializer.jar \
	/usr/share/java/xercesImpl.jar \
	/usr/share/java/xml-apis.jar

# Implementations (or at least APIs) of standards
dep-std-jsr305 = build/packages/jsr305                # "Annotations for Software Defect Detection"
dep-std-jsr315 = $(dep-std-servlet30)                 # "Java Servlet 3.0 Specification"
dep-std-jsr330 = build/packages/atinject-javax.inject # "Dependency Injection for Java"
dep-std-jsr340 = $(dep-std-servlet31)                 # "Java Servlet 3.1 Specification"

dep-std-jpa10 = $(dep-std-jpa20)
dep-std-jpa20 = build/packages/eclipselink-persistence20
dep-std-jpa21 = build/packages/eclipselink-persistence21

dep-std-servlet25 = /usr/share/java/tomcat6/servlet-api.jar
dep-std-servlet30 = /usr/share/java/tomcat7/servlet-api.jar
dep-std-servlet31 = /usr/share/java/tomcat8/servlet-api.jar

dep-std-javamail = /usr/share/java/gnumail.jar

################################################################################

# Apache (from tarballs)

build/packages/httpcomponents-core: \
	build/packages/commons-cli
build/packages/httpcomponents-client-bootstrap: \
	build/packages/commons-codec \
	build/packages/commons-logging-bootstrap \
	build/packages/httpcomponents-core

build/packages/maven: \
	$(dep-std-jsr330) \
	build/packages/plexus-metadata-generator-cli \
	build/packages/modello-cli \
	build/packages/aether-core \
	build/packages/aopalliance \
	build/packages/cglib \
	build/packages/commons-cli \
	build/packages/guava \
	build/packages/guice-bootstrap \
	build/packages/logback-core \
	build/packages/modello-plugins \
	build/packages/plexus-cipher \
	build/packages/plexus-classworlds \
	build/packages/plexus-component-annotations \
	build/packages/plexus-interpolation \
	build/packages/plexus-sec-dispatcher \
	build/packages/plexus-utils \
	build/packages/sisu-inject-bootstrap \
	build/packages/sisu-plexus \
	build/packages/slf4j-api \
	build/packages/slf4j-simple \
	build/packages/wagon
build/packages/maven-artifact-bootstrap: \
	build/packages/plexus-utils
build/packages/maven-plugin-api-bootstrap: \
	build/packages/maven-artifact-bootstrap \
	build/packages/plexus-classworlds \
	build/packages/plexus-container-default \
	build/packages/plexus-utils

# Apache (from SVN)
build/packages/maven-plugin-annotations-bootstrap: \
	build/packages/maven-artifact-bootstrap
build/packages/maven-plugin-tools-api-bootstrap: \
	build/packages/maven-artifact-bootstrap \
	build/packages/maven-plugin-api-bootstrap \
	build/packages/plexus-container-default \
	build/packages/plexus-utils
build/packages/xbean-bootstrap: \
	build/packages/asm3 \
	build/packages/commons-logging-bootstrap \
	build/packages/osgi42-core \
	build/packages/slf4j-api

# Modello
build/packages/modello-core: \
	build/packages/plexus-build-api \
	build/packages/plexus-container-default \
	build/packages/plexus-utils
build/packages/modello-plugins: \
	build/packages/modello-core \
	build/packages/plexus-container-default \
	build/packages/plexus-build-api \
	build/packages/plexus-utils
build/packages/modello-cli: \
	build/packages/guava \
	build/packages/modello-core \
	build/packages/plexus-build-api \
	build/packages/plexus-classworlds \
	build/packages/plexus-container-default \
	build/packages/plexus-utils \
	build/packages/xbean-bootstrap

# Plexus
build/packages/plexus-build-api: \
	build/packages/plexus-container-default \
	build/packages/plexus-utils
build/packages/plexus-cipher: \
	$(dep-std-jsr330)
build/packages/plexus-cli: \
	build/packages/commons-cli \
	build/packages/plexus-classworlds \
	build/packages/plexus-container-default
build/packages/plexus-interactivity: \
	build/packages/jline \
	build/packages/plexus-container-default \
	build/packages/plexus-utils
build/packages/plexus-sec-dispatcher: \
	build/packages/modello-cli \
	build/packages/modello-plugins \
	build/packages/plexus-cipher \
	build/packages/plexus-container-default
build/packages/plexus-component-metadata-bootstrap: \
	build/packages/asm3 \
	build/packages/commons-cli \
	build/packages/jdom1 \
	build/packages/maven-plugin-api-bootstrap \
	build/packages/plexus-classworlds \
	build/packages/plexus-cli \
	build/packages/plexus-component-annotations \
	build/packages/plexus-container-default \
	build/packages/plexus-utils \
	build/packages/qdox
build/packages/plexus-container-default: \
	/usr/share/java/junit.jar \
	build/packages/guava \
	build/packages/plexus-classworlds \
	build/packages/plexus-utils \
	build/packages/xbean-bootstrap
build/packages/plexus-metadata-generator-cli: \
	build/packages/asm3 \
	build/packages/commons-cli \
	build/packages/guava \
	build/packages/jdom1 \
	build/packages/maven-plugin-api-bootstrap \
	build/packages/plexus-classworlds \
	build/packages/plexus-cli \
	build/packages/plexus-component-metadata-bootstrap \
	build/packages/plexus-container-default \
	build/packages/plexus-utils \
	build/packages/qdox \
	build/packages/xbean-bootstrap

# OSGi
build/packages/osgi50-enterprise: \
	$(dep-std-jpa21) \
	$(dep-std-servlet31) \
	build/packages/osgi50-core

# ASM
build/packages/ow-util-ant-tasks: build/packages/asm2
#build/packages/asm1: build/packages/ow-util-ant-tasks
build/packages/asm2: build/packages/ow-util-ant-tasks-bootstrap
build/packages/asm3: build/packages/ow-util-ant-tasks
build/packages/asm4: build/packages/ow-util-ant-tasks build/packages/bnd
#build/packages/asm5: build/packages/ow-util-ant-tasks build/packages/bnd

# Quality Open Software
build/packages/logback-core: \
	$(dep-std-javamail) \
	$(dep-std-servlet25) \
	build/packages/janino \
	build/packages/jansi
build/packages/slf4j-simple: \
	build/packages/slf4j-api

# Eclipse
build/packages/aether-core: \
	build/packages/guice-bootstrap \
	build/packages/httpcomponents-client-bootstrap \
	build/packages/httpcomponents-core \
	build/packages/plexus-classworlds \
	build/packages/plexus-component-annotations \
	build/packages/plexus-container-default \
	build/packages/plexus-utils \
	build/packages/sisu-inject-bootstrap \
	build/packages/slf4j-api \
	build/packages/wagon
build/packages/eclipselink-persistence20: \
	build/packages/osgi50-core # any version would probably work
build/packages/eclipselink-persistence21: \
	build/packages/osgi50-core # any version would probably work
build/packages/sisu-inject-bootstrap: \
	/usr/share/java/junit.jar \
	$(dep-std-servlet25) \
	build/packages/guice-bootstrap \
	build/packages/osgi50-core \
	build/packages/slf4j-api
build/packages/sisu-plexus: \
	/usr/share/java/junit.jar \
	$(dep-std-jsr330) \
	build/packages/guice-bootstrap \
	build/packages/osgi50-core \
	build/packages/plexus-classworlds \
	build/packages/plexus-component-annotations \
	build/packages/plexus-utils \
	build/packages/sisu-inject-bootstrap \
	build/packages/slf4j-api

# FuseSource
build/packages/jansi: \
	build/packages/jansi-native
build/packages/jansi-native: \
	build/packages/hawtjni-bootstrap
build/packages/hawtjni-bootstrap: \
	build/packages/commons-cli \
	build/packages/xbean-bootstrap

# Misc (from tarballs)
build/packages/bnd: \
	/opt/apache-ant/lib/ant.jar \
	/usr/share/java/junit.jar \
	build/packages/osgi50-core \
	build/packages/osgi50-enterprise
build/packages/cglib: \
	build/packages/asm4 \
	build/packages/jarjar
build/packages/guice-bootstrap: \
	$(dep-std-jsr330) \
	$(dep-std-jpa10) \
	$(dep-std-servlet25) \
	build/packages/aopalliance \
	build/packages/asm3 \
	build/packages/cglib
build/packages/qdox: \
	build/packages/jflex
build/packages/wagon: \
	/usr/share/java/junit.jar \
	build/packages/commons-io \
	build/packages/commons-lang2 \
	build/packages/commons-net \
	build/packages/httpcomponents-core \
	build/packages/httpcomponents-client-bootstrap \
	build/packages/jsoup \
	build/packages/plexus-interactivity \
	build/packages/plexus-utils

# Misc (from VCS)
build/packages/guava: \
	$(dep-std-jsr305)
build/packages/jarjar: \
	build/packages/asm4 \
	build/packages/maven-plugin-api-bootstrap

# Misc individual things I decided to give multiple lines to in sources.mk

build/packages/janino: \
	/opt/apache-ant/lib/ant.jar

# JDOM/Jaxen
build/packages/jdom1: \
	build/packages/jaxen-bootstrap \
	$(dep-meta-xerces2)
build/packages/jaxen-bootstrap: \
	build/packages/jdom1-bootstrap
build/packages/jdom1-bootstrap: \
	$(dep-meta-xerces2)

# JFlex
build/packages/jflex: \
	/usr/share/java/junit.jar \
	build/packages/jflex-bootstrap \
	build/packages/java_cup-cli
build/packages/jflex-bootstrap: \
	/usr/share/java/junit.jar \
	build/packages/java_cup-cli \
	build/packages/jlex-cli

build/packages/java_cup-cli: build/packages/java_cup
build/packages/jlex-cli: build/packages/jlex
