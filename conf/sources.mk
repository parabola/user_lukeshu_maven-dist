_apache_svn                                 =  https://svn.apache.org/repos/asf
_apache_mirror                              =  http://archive.apache.org/dist
_objectweb_mirror                           =  http://download.forge.objectweb.org

_tarbombs =

# Apache tarballs

commons-cli                                 =  tar|$(_apache_mirror)/commons/cli/source/commons-cli-1.2-src.tar.gz
commons-codec                               =  tar|$(_apache_mirror)/commons/codec/source/commons-codec-1.9-src.tar.gz
commons-io                                  =  tar|$(_apache_mirror)/commons/io/source/commons-io-2.4-src.tar.gz
commons-lang2                               =  tar|$(_apache_mirror)/commons/lang/source/commons-lang-2.6-src.tar.gz
commons-logging-bootstrap                   =  tar|$(_apache_mirror)/commons/logging/source/commons-logging-1.2-src.tar.gz
commons-net                                 =  tar|$(_apache_mirror)/commons/net/source/commons-net-3.3-src.tar.gz

httpcomponents-core                         =  tar|$(_apache_mirror)/httpcomponents/httpcore/source/httpcomponents-core-4.3.2-src.tar.gz
httpcomponents-client-bootstrap             =  tar|$(_apache_mirror)/httpcomponents/httpclient/source/httpcomponents-client-4.3.4-src.tar.gz

maven                                       =  tar|$(_apache_mirror)/maven/maven-3/3.2.1/source/apache-maven-3.2.1-src.tar.gz
maven-artifact-bootstrap                    =  tar|$(_apache_mirror)/maven/maven-3/3.2.1/source/apache-maven-3.2.1-src.tar.gz|maven-artifact
maven-plugin-api-bootstrap                  =  tar|$(_apache_mirror)/maven/maven-3/3.2.1/source/apache-maven-3.2.1-src.tar.gz|maven-plugin-api

# Apache SVN
xbean-bootstrap                             =  svn|$(_apache_svn)/geronimo/xbean|tags/xbean-3.9

# Modello
modello-core                                =  tar|https://github.com/sonatype/modello/archive/modello-1.8.1.tar.gz|modello-core
modello-plugins                             =  tar|https://github.com/sonatype/modello/archive/modello-1.8.1.tar.gz|modello-plugins
modello-cli                                 =  stub|modello|org.codehaus.modello.ModelloCli

# Plexus
plexus-build-api                            =  tar|https://github.com/sonatype/sisu-build-api/archive/plexus-build-api-0.0.7.tar.gz
plexus-cipher                               =  tar|https://github.com/sonatype/plexus-cipher/archive/plexus-cipher-1.7.tar.gz
plexus-classworlds                          =  tar|https://github.com/sonatype/plexus-classworlds/archive/plexus-classworlds-2.5.1.tar.gz
plexus-cli                                  =  tar|https://github.com/sonatype/plexus-components/archive/plexus-components-1.1.20.tar.gz|plexus-cli
plexus-interactivity                        =  tar|https://github.com/sonatype/plexus-components/archive/plexus-interactivity-1.0-alpha-6.tar.gz
plexus-interpolation                        =  tar|https://github.com/sonatype/plexus-interpolation/archive/plexus-interpolation-1.19.tar.gz
plexus-sec-dispatcher                       =  git|git://github.com/sonatype/plexus-sec-dispatcher.git|7db8f880486e192a1c5ea9544e01e756c3d49d0f
plexus-utils                                =  tar|https://github.com/sonatype/plexus-utils/archive/plexus-utils-3.0.17.tar.gz
plexus-metadata-generator-cli               =  stub|plexus-metadata-generator|org.codehaus.plexus.metadata.PlexusMetadataGeneratorCli
# Plexus containers
plexus-component-annotations                =  tar|https://github.com/sonatype/plexus-containers/archive/plexus-containers-1.5.5.tar.gz|plexus-component-annotations
plexus-component-metadata-bootstrap         =  tar|https://github.com/sonatype/plexus-containers/archive/plexus-containers-1.5.5.tar.gz|plexus-component-metadata
plexus-container-default                    =  tar|https://github.com/sonatype/plexus-containers/archive/plexus-containers-1.5.5.tar.gz|plexus-container-default

# OSGi code (starting at 4.2)
osgi42-core                                 =  tar|http://www.osgi.org/download/r4v42/osgi.core.jar|OSGI-OPT
#osgi42-cmpn                                 =  tar|http://www.osgi.org/download/r4v42/osgi.cmpn.jar|OSGI-OPT
#osgi42-enterprise                           =  tar|http://www.osgi.org/download/r4v42/osgi.enterprise.jar|OSGI-OPT
_tarbombs += $(osgi42-core) $(osgi42-cmpn) $(osgi42-enterprise)

#osgi43-core                                 =  tar|http://www.osgi.org/download/r4v43/osgi.core-4.3.1.jar|OSGI-OPT
#osgi43-cmpn                                 =  tar|http://www.osgi.org/download/r4v43/osgi.cmpn-4.3.1.jar|OSGI-OPT
#osgi43-residential                          =  tar|http://www.osgi.org/download/r4v43/osgi.residential-4.3.0.jar|OSGI-OPT
_tarbombs += $(osgi43-core) $(osgi43-cmpn) $(osgi43-residential)

osgi50-core                                 =  tar|http://www.osgi.org/download/r5/osgi.core-5.0.0.jar|OSGI-OPT
#osgi50-cmpn                                 =  tar|http://www.osgi.org/download/r5/osgi.cmpn-5.0.0.jar|OSGI-OPT
osgi50-enterprise                           =  tar|http://www.osgi.org/download/r5/osgi.enterprise-5.0.0.jar|OSGI-OPT
_tarbombs += $(osgi50-core) $(osgi50-cmpn) $(osgi50-enterprise)

#osgi60-core                                 =  tar|http://www.osgi.org/download/r6/osgi.core-6.0.0.jar|OSGI-OPT
_tarbombs += $(osgi60-core)

# ObjectWeb
#asm1                                        =  tar|$(_objectweb_mirror)/asm/asm-1.5.3.tar.gz
asm2                                        =  tar|$(_objectweb_mirror)/asm/asm-2.2.3.tar.gz
asm3                                        =  tar|$(_objectweb_mirror)/asm/asm-3.3.1.tar.gz
asm4                                        =  tar|$(_objectweb_mirror)/asm/asm-4.2.tar.gz
#asm5                                        =  tar|$(_objectweb_mirror)/asm/asm-5.0.3.tar.gz
ow-util-ant-tasks                           =  tar|$(_objectweb_mirror)/monolog/ow_util_ant_tasks_1.3.2.zip
ow-util-ant-tasks-bootstrap                 =  $(ow-util-ant-tasks)
_tarbombs += $(ow-util-ant-tasks)

# Quality Open Software
logback-core                                =  tar|http://logback.qos.ch/dist/logback-1.1.2.tar.gz|logback-core # HTTPS is mis-configured
slf4j-api                                   =  tar|http://www.slf4j.org/dist/slf4j-1.7.7.tar.gz|slf4j-api # HTTPS is mis-configured
slf4j-simple                                =  tar|http://www.slf4j.org/dist/slf4j-1.7.7.tar.gz|slf4j-simple # HTTPS is mis-configured

# Eclipse
aether-core                                 =  tar|https://git.eclipse.org/c/aether/aether-core.git/snapshot/aether-1.0.0.v20140518.tar.gz
eclipselink-persistence20                   =  tar|https://git.eclipse.org/c/eclipselink/javax.persistence.git/snapshot/javax.persistence-2.0.6.v201308211412.tar.gz
eclipselink-persistence21                   =  tar|https://git.eclipse.org/c/eclipselink/javax.persistence.git/snapshot/javax.persistence-2.1.0.v201304241213.tar.gz
sisu-inject-bootstrap                       =  tar|https://git.eclipse.org/c/sisu/org.eclipse.sisu.inject.git/snapshot/releases/0.2.1.tar.gz|0.2.1
sisu-plexus                                 =  tar|https://git.eclipse.org/c/sisu/org.eclipse.sisu.plexus.git/snapshot/releases/0.2.1.tar.gz|0.2.1

# FuseSource
jansi                                       =  tar|https://github.com/fusesource/jansi/archive/jansi-project-1.11.tar.gz
jansi-native                                =  tar|https://github.com/fusesource/jansi-native/archive/jansi-native-1.5.tar.gz
hawtjni-bootstrap                           =  tar|https://github.com/fusesource/hawtjni/archive/hawtjni-project-1.10.tar.gz

# Misc tarballs
aopalliance                                 =  tar|http://aopalliance.cvs.sourceforge.net/viewvc/aopalliance/aopalliance/?view=tar
bnd                                         =  tar|https://github.com/bndtools/bnd/archive/2.3.0.REL.tar.gz
cglib                                       =  tar|https://github.com/cglib/cglib/archive/RELEASE_3_1.tar.gz
guice-bootstrap                             =  tar|https://google-guice.googlecode.com/files/guice-3.0-src.zip
#javacc                                      =  tar|https://java.net/projects/javacc/downloads/download/javacc-6.0src.zip
jline                                       =  tar|https://downloads.sourceforge.net/project/jline/jline/1.0/jline-1.0.zip|src
qdox                                        =  tar|https://nexus.codehaus.org/content/repositories/releases/com/thoughtworks/qdox/qdox/2.0-M1/qdox-2.0-M1-project.tar.bz2
wagon                                       =  tar|$(_apache_mirror)/maven/wagon/wagon-2.6-source-release.zip

# Misc VCS
atinject-javax.inject                       =  svn|https://atinject.googlecode.com/svn|trunk
guava                                       =  git|https://code.google.com/p/guava-libraries/|v17.0/guava
jarjar                                      =  svn|https://jarjar.googlecode.com/svn|trunk/jarjar
jsoup                                       =  git|git://github.com/jhy/jsoup.git|jsoup-1.7.3
jsr305                                      =  svn|https://jsr-305.googlecode.com/svn|trunk/ri

# Misc individual things I decided to give multiple lines to

_janino-part1                               =  svn|https://svn.codehaus.org/janino|tags/janino_2.7.5
_janino-part2                               =  svn|https://svn.code.sf.net/p/loggifier/code|tags/janino_2.7.5
janino                                      =  union|_janino-part1:_janino-part2

# Jaxen is used to bootstrap jdom1
jdom1                                       =  tar|https://github.com/hunterhacker/jdom/archive/jdom-1.1.3.tar.gz
jdom1-bootstrap                             =  $(jdom1)
jaxen-bootstrap                             =  tar|http://dist.codehaus.org/jaxen/distributions/jaxen-1.1.6-src.tar.gz # HTTPS is broken

jflex                                       =  tar|http://jflex.de/jflex-1.5.1.tar.gz # No TLS
jflex-bootstrap                             =  $(jflex)

# This is the last version of CUP developed at Princeton;  I have a low opinion of the maintainers at TUM.
#java_cup                                    =  tar|http://www.cs.princeton.edu/~appel/modern/java/CUP/java_cup_v10k.tar.gz
java_cup                                    =  tar|http://www2.cs.tum.edu/projects/cup/java_cup_v10k.tar.gz
jlex                                        =  file|http://www.cs.princeton.edu/~appel/modern/java/JLex/Archive/1.2.6/Main.java
java_cup-cli                                =  stub|java_cup|java_cup.Main
jlex-cli                                    =  stub|jlex|JLex.Main
