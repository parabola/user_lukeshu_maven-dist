
_apache_svn                                 =  https://svn.apache.org/repos/asf
_apache_mirror                              =  http://archive.apache.org/dist

maven                                       =  tar|$(_apache_mirror)/maven/maven-3/3.2.1/source/apache-maven-3.2.1-src.tar.gz
maven-extras-enforcer                       =  svn|$(_apache_svn)/maven/enforcer|tags/enforcer-1.3
maven-extras-maven-clean-plugin             =  svn|$(_apache_svn)/maven/plugins|tags/maven-clean-plugin-2.5
maven-extras-maven-compiler-plugin          =  svn|$(_apache_svn)/maven/plugins|tags/maven-compiler-plugin-3.1
maven-extras-maven-deploy-plugin            =  svn|$(_apache_svn)/maven/plugins|tags/maven-deploy-plugin-2.8.1
maven-extras-maven-install-plugin           =  svn|$(_apache_svn)/maven/plugins|tags/maven-install-plugin-2.5.1
maven-extras-maven-plugins                  =  svn|$(_apache_svn)/maven/plugins|tags/maven-plugins-24
maven-extras-maven-remote-resources-plugin  =  svn|$(_apache_svn)/maven/plugins|tags/maven-remote-resources-plugin-1.5
maven-extras-maven-resources-plugin         =  svn|$(_apache_svn)/maven/plugins|tags/maven-resources-plugin-2.6
maven-extras-maven-site-plugin              =  svn|$(_apache_svn)/maven/plugins|tags/maven-site-plugin-3.3
maven-extras-maven-verifier-plugin          =  svn|$(_apache_svn)/maven/plugins|tags/maven-verifier-plugin-1.0
maven-extras-plugin-tools                   =  svn|$(_apache_svn)/maven/plugin-tools|tags/maven-plugin-tools-3.3
maven-extras-pom-asf                        =  svn|$(_apache_svn)/maven/pom|tags/apache-13
maven-extras-pom-maven                      =  svn|$(_apache_svn)/maven/pom|tags/maven-parent-23

geronimo-jms_1.1_spec                       =  svn|$(_apache_svn)/geronimo/specs|tags/geronimo-jms_1.1_spec-1.1.1
xbean-reflect                               =  svn|$(_apache_svn)/geronimo/xbean|tags/xbean-3.4/xbean-reflect # yes, I know xbean 3.4 is old

commons-lang3                               =  tar|$(_apache_mirror)/commons/lang/source/commons-lang3-3.3.2-src.tar.gz

plexus-archiver                             =  git|git://github.com/sonatype/plexus-archiver.git|plexus-archiver-2.4.3
plexus-cipher                               =  git|git://github.com/sonatype/plexus-cipher.git|plexus-cipher-1.7
plexus-classworlds                          =  git|git://github.com/sonatype/plexus-classworlds.git|plexus-classworlds-2.5.1
plexus-compiler                             =  git|git://github.com/sonatype/plexus-compiler.git|plexus-compiler-2.3
plexus-containers                           =  git|git://github.com/sonatype/plexus-containers.git|plexus-containers-1.5.5
plexus-interpolation                        =  git|git://github.com/sonatype/plexus-interpolation.git|plexus-interpolation-1.19
plexus-io                                   =  git|git://github.com/sonatype/plexus-io.git|plexus-io-2.0.9
plexus-pom                                  =  git|git://github.com/sonatype/plexus-pom.git|plexus-3.3.1
plexus-utils                                =  git|git://github.com/sonatype/plexus-utils.git|plexus-utils-3.0.17
atinject-javax.inject-tck                   =  svn|http://atinject.googlecode.com/svn|tags/javax.inject-tck-1

