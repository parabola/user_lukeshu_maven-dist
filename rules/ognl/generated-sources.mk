JJTREE = $(JAVA) org.javacc.jjtree.Main
JAVACC = $(JAVA) org.javacc.parser.Main

target/generated-sources: target/generated-sources/ognl
target/generated-sources/ognl: src/java/ognl/ognl.jjt
	$(RM) -r $@
	$(JJTREE) -BUILD_NODE_FILES=false -OUTPUT_DIRECTORY=$@ $< || { $(RM) -r $@; $(FAIL); }
	$(JAVACC) -OUTPUT_DIRECTORY=$@ $@/ognl.jj || { $(RM) -r $@; $(FAIL); }
	$(RM) $@/Node.java
