modello-plugin-xdoc_deps       = modello-plugin-xml modello-plugin-xsd
modello-plugin-xsd_deps        = modello-plugin-xml

modello-plugin-converters_deps = modello-plugin-java
modello-plugin-xml_deps        = modello-plugin-java

modello-plugin-dom4j_deps      = modello-plugin-java modello-plugin-xml
modello-plugin-jackson_deps    = modello-plugin-java modello-plugin-xml
modello-plugin-jdom_deps       = modello-plugin-java modello-plugin-xml
modello-plugin-sax_deps        = modello-plugin-java modello-plugin-xml
modello-plugin-snakeyaml_deps  = modello-plugin-java modello-plugin-xml
modello-plugin-stax_deps       = modello-plugin-java modello-plugin-xml

# FIXME: generated code?
modello-plugin-xpp3_deps       = modello-plugin-java modello-plugin-xml

# FIXME: depends on an external 'fasterxml'
subdirs := $(filter-out modello-plugin-jsonschema,$(subdirs))
