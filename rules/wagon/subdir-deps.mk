wagon-providers_deps = wagon-provider-api

wagon-http-lightweight_deps = wagon-http-shared
wagon-http_deps = wagon-http-shared

subdirs := $(filter-out wagon-%-test wagon-tcks,$(subdirs))

subdirs := $(filter-out wagon-ssh-external,$(subdirs)) # FIXME: depends on mina-ssh
subdirs := $(filter-out wagon-scm,$(subdirs)) # FIXME: depends on org.apache.maven.scm
subdirs := $(filter-out wagon-ssh,$(subdirs)) # FIXME: depends on jsch, other stuff, idk
subdirs := $(filter-out wagon-webdav-jackrabbit,$(subdirs)) # FIXME: depends on Apache jackrabbit
