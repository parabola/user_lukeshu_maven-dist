aether-impl_deps      = aether-api aether-spi aether-util
aether-spi_deps       = aether-api
aether-test-util_deps = aether-api aether-spi
aether-util_deps      = aether-api

aether-connector-basic_deps     = aether-api aether-spi aether-util
aether-transport-classpath_deps = aether-api aether-spi aether-util
aether-transport-file_deps      = aether-api aether-spi
aether-transport-http_deps      = aether-api aether-spi aether-util
aether-transport-wagon_deps     = aether-api aether-spi aether-util
