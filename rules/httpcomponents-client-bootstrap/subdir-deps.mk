fluent-hc_deps        = httpclient
httpclient-cache_deps = httpclient
httpclient-osgi_deps  = httpclient
httpclient-win_deps   = httpclient

subdirs := $(filter-out httpclient-osgi,$(subdirs)) # depends on an OSGi framework implementation
subdirs := $(filter-out httpclient-win,$(subdirs)) # depends on native Windows JNI crud

sourceDirectory += src/main/java-deprecated
