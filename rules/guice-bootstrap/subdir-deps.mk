# top level
extensions_deps = core

# extensions
struts2_deps = servlet
subdirs := $(filter-out struts2,$(subdirs)) # depends on struts2, which depends on Spring, TestNG
subdirs := $(filter-out spring,$(subdirs)) # depends on Spring
