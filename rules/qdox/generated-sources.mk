findjar = $(firstword $(foreach path,$(subst :, ,$(CLASSPATH)),$(if $(findstring $1,$(path)),$(path))) /notfound.jar)

BYACCJ = byaccj
JFLEX = $(JAVA) -jar $(call findjar,jflex)
qdox.javaparser.stack = 500

parserpkg = com.thoughtworks.qdox.parser.impl
parserdir = target/generated-sources/parser/$(subst .,/,$(parserpkg))
byaccj_flags = -Jnorun -Jnoconstruct -Jpackage=$(parserpkg)

target/generated-sources: $(addprefix $(parserdir)/,DefaultJavaCommentLexer.java JFlexLexer.java DefaultJavaCommentParser.java DefaultJavaCommentParserVal.java Parser.java)

$(parserdir)/DefaultJavaCommentLexer.java: src/grammar/commentlexer.flex
	$(JFLEX) -d $(@D) $<

$(parserdir)/JFlexLexer.java: src/grammar/lexer.flex
	$(JFLEX) -d $(@D) $<

$(addprefix $(parserdir)/DefaultJavaCommentParser,.java Val.java): src/grammar/commentparser.y
	$(MKDIRS) $(@D)
	cd $(@D) && $(BYACCJ) $(byaccj_flags) -Jclass=DefaultJavaCommentParser ${basedir}/$<

$(parserdir)/Parser.java: src/grammar/parser.y
	$(MKDIRS) $(@D)
	cd $(@D) && $(BYACCJ) $(byaccj_flags) -Jclass=Parser -Jimplements=CommentHandler -Jsemantic=Value -Jstack=${qdox.javaparser.stack} ${basedir}/$<
