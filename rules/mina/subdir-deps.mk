mina-filter-compression_deps = mina-core
mina-http_deps               = mina-core
mina-integration-beans_deps  = mina-core
mina-integration-jmx_deps    = mina-core mina-integration-beans mina-integration-ognl
mina-integration-ognl_deps   = mina-core mina-integration-beans
mina-integration-xbean_deps  = mina-core mina-integration-beans
mina-statemachine_deps       = mina-core
mina-transport-apr_deps      = mina-core
mina-transport-serial_deps   = mina-core mina-integration-beans

subdirs := $(filter-out mina-example,$(subdirs))
subdirs := $(filter-out mina-integration-xbean,$(subdirs)) # FIXME: depends on org.springframework
