org.eclipse.sisu.inject.extender_deps = org.eclipse.sisu.inject
org.eclipse.sisu.inject.site_deps = org.eclipse.sisu.inject
org.eclipse.sisu.inject.tests_deps = org.eclipse.sisu.inject

subdirs := $(filter-out org.eclipse.sisu.inject.tests,$(subdirs))
